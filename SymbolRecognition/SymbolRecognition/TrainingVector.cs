﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SymbolRecognition
{ 
    class TrainingVector
    {
        private double[,] Inputs;
        private Boolean[] Answers;
        private int NumOfInputs=0;
        private String Symbol;
        private int CanvasNetWidth;
        private int CanvasNetHeight;
        private int AnswersQuantity;
        public double[] OutputValues;

        public TrainingVector(TrainingVector t)
        {
            this.Answers = t.Answers;
            this.Inputs = t.Inputs;
            this.CanvasNetHeight = t.CanvasNetHeight;
            this.CanvasNetWidth = t.CanvasNetWidth;
            this.Symbol = t.Symbol;
            this.OutputValues = new double[AnswersQuantity];
            this.GetNumOfInputs();
        }


        public TrainingVector(int canvasNetWidth, int canvasNetHeight, int answersQuantity)
        {
            this.CanvasNetHeight = canvasNetHeight;
            this.CanvasNetWidth = canvasNetWidth;
            this.AnswersQuantity = answersQuantity;
            this.OutputValues = new double[AnswersQuantity];
            ClearAll();  
        }
        public TrainingVector(int canvasNetWidth, int canvasNetHeight, int answersQuantity, String Symbol, double[,] Inputs, Boolean[] Answers)
        {
            this.CanvasNetHeight = canvasNetHeight;
            this.CanvasNetWidth = canvasNetWidth;
            this.AnswersQuantity = answersQuantity;
            this.OutputValues = new double[AnswersQuantity];
            this.Symbol = Symbol;
            this.Inputs = Inputs;
            this.Answers = Answers;
        }
        public double[,] GetInputs()
        {
            return Inputs;
        }

        public double GetInput(int x, int y)
        {
            return Inputs[x, y];
        }

        public void SetNumOfInputs(int num)
        {
            NumOfInputs = num;
        }

        public int GetNumOfInputs()
        {
            NumOfInputs = 0;
            for(int i=0; i<CanvasNetWidth; i++)
            {
                for(int j=0; j<CanvasNetHeight; j++)
                {
                    if (GetInput(i, j)>0) NumOfInputs++;
                }
            }
            return NumOfInputs;
        }

        public void SetInputs(double[,] inputs)
        {
            this.Inputs = inputs;
        }
        public void SetInput(int row, int col, double input)
        { 
            Inputs[row, col] = input;
        }
        public Boolean[] GetAnswers()
        {
            return Answers;
        }
        public Boolean GetAnswer(int x)
        {
            return Answers[x];
        }
        public void SetAnswers(Boolean[] answers)
        {
            this.Answers = answers;
        }
        public void SetAnswer(int i, Boolean input)
        {
            Answers[i] = input;
        }
        public String getSymbol()
        {
            return Symbol;
        }
        public void SetSymbol(String symbol)
        {
            this.Symbol = symbol;
        }
        public void ClearAll()
        {
            Symbol = "";
            NumOfInputs = 0;
            Inputs = new double[CanvasNetHeight, CanvasNetWidth];
            Answers = new Boolean[AnswersQuantity];
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    Inputs[i, j] = 0;
                }
            }
            for (int j = 0; j < 5; j++)
            {
                Answers[j] = false;
            }
        }
    }
}
