﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SymbolRecognition
{
    /*
    Klasa trenująca sieć neuronową
     */
    class NeuralNetwork
    {
        public TrainingVector TempVector;
        public List<TrainingVector> TrainingVectorList = new List<TrainingVector>();
        public int canvasNetWidth = 9; //Szerokość siatki pod canvasem
        public int canvasNetHeight = 9; //Wysokość siatki pod canvasem
        public int answersQuantity = 5; //Ilość bitów poprawnych odpowiedzi
        private double accuracy = 0.1;
        private double canvasWidth;
        private int WidthModulo;
        private double canvasHeight;
        private int HeightModulo;
        //Zmiennie dla trenowania sieci
        private int InputNeuronsCount;
        private int MiddleNeuronsCount;
        private int OutputNeuronsCount;
        private double[] OutputValue;
        private List<Neuron> InputNeurons = new List<Neuron>();
        private List<Neuron> MiddleNeurons = new List<Neuron>();
        private List<Neuron> OutputNeurons = new List<Neuron>();
        private double p = 0.2;
        private double alpha = 0.9;
        private double[] Error;
        public double ErrorAVG { get; set; } = 99;
        private double MinError = 0.02;
        private int StepCount;
        public bool cancel { get; set; } = false;

        public void SetNetwork(Double width, Double height)
        {
            TempVector = new TrainingVector(canvasNetWidth, canvasNetHeight, answersQuantity);
            canvasWidth = width;
            canvasHeight = height;
            WidthModulo = (int)canvasWidth / canvasNetWidth;
            HeightModulo = (int)canvasHeight / canvasNetHeight;
            InputNeuronsCount = canvasNetWidth * canvasNetHeight;
            OutputNeuronsCount = answersQuantity;
            MiddleNeuronsCount = (int)Math.Sqrt(InputNeuronsCount * OutputNeuronsCount);
            Random rand = new Random((int)DateTime.Now.Ticks);
            for (int i = 0; i < InputNeuronsCount; i++)
            {
                InputNeurons.Add(new Neuron(1, rand));
            }
            for (int i = 0; i < MiddleNeuronsCount; i++)
            {
                MiddleNeurons.Add(new Neuron(InputNeuronsCount, rand));
            }
            for (int i = 0; i < OutputNeuronsCount; i++)
            {
                OutputNeurons.Add(new Neuron(MiddleNeuronsCount, rand));
            }

            OutputValue = new double[OutputNeuronsCount];
        }

        public void TrainNetwork(Label status, Window main, ProgressBar progbar)
        {
            //Thread.Sleep(TimeSpan.FromSeconds(5));
            cancel = false;
                main.Dispatcher.BeginInvoke(new Action(() =>
                {
                    progbar.Maximum = 0.5 - MinError;
                }));
                StepCount = 0;
                while (ErrorAVG > MinError && !cancel)
                {
                    foreach (TrainingVector t in TrainingVectorList.ToList())
                    {
                        //Zerowanie sum wazonych
                        foreach (Neuron n in InputNeurons.ToList())
                        {
                            n.WeightedSum = 1;
                        }
                        foreach (Neuron n in MiddleNeurons.ToList())
                        {
                            n.WeightedSum = 1;
                        }
                        foreach (Neuron n in OutputNeurons.ToList())
                        {
                            n.WeightedSum = 1;
                        }

                        //Faza propagacji w przód
                        foreach (Neuron n in InputNeurons.ToList())
                        {
                            int i = InputNeurons.IndexOf(n) / canvasNetWidth, j = InputNeurons.IndexOf(n) % canvasNetWidth;
                            n.CalcWeightedSum(1, t.GetInput(i, j));
                            n.OutputFunction();
                        }
                        foreach (Neuron n in MiddleNeurons.ToList())
                        {
                            int index = 1;
                            foreach (Neuron i in InputNeurons.ToList())
                            {
                                n.CalcWeightedSum(index, i.Output);
                                index++;
                            }
                            n.OutputFunction();
                        }
                        foreach (Neuron n in OutputNeurons.ToList())
                        {
                            int index = 1;
                            foreach (Neuron i in MiddleNeurons.ToList())
                            {
                                n.CalcWeightedSum(index, i.Output);
                                index++;
                            }
                            n.OutputFunction();
                        }
                        //Faza propagacji w tył
                        foreach (Neuron n in OutputNeurons.ToList())
                        {
                            int C;
                            if (t.GetAnswer(OutputNeurons.IndexOf(n)))
                            {
                                C = 1;
                            }
                            else
                            {
                                C = 0;
                            }
                            n.Delta = (C - n.Output) * n.Deriv;
                        }

                        foreach (Neuron n in MiddleNeurons.ToList())
                        {
                            n.Delta = 0;
                            foreach (Neuron m in OutputNeurons.ToList())
                            {
                                n.Delta += m.Delta * m.InputWeights[MiddleNeurons.IndexOf(n) + 1];
                            }
                        }

                        foreach (Neuron n in InputNeurons.ToList())
                        {
                            n.Delta = 0;
                            foreach (Neuron m in MiddleNeurons.ToList())
                            {
                                n.Delta += m.Delta * m.InputWeights[InputNeurons.IndexOf(n) + 1];
                            }
                        }
                        //Uaktualnienie wag
                        foreach (Neuron n in InputNeurons.ToList())
                        {
                            int i = InputNeurons.IndexOf(n) / canvasNetWidth, j = InputNeurons.IndexOf(n) % canvasNetWidth;
                        //double tempWeight = n.InputWeights[1] + p * n.Delta * t.GetInput(i, j);
                        //double deltaWeight = tempWeight - n.InputWeights[1];
                        //n.InputWeights[1] = n.InputWeights[1] + alpha * deltaWeight + p * n.Delta * t.GetInput(i, j);
                            n.InputWeights[1] = n.InputWeights[1] + p * n.Delta * t.GetInput(i, j);
                    }

                        foreach (Neuron n in MiddleNeurons.ToList())
                        {
                            foreach (Neuron m in InputNeurons.ToList())
                            {
                            //double tempWeight = n.InputWeights[InputNeurons.IndexOf(m) + 1] + p * n.Delta * m.Output;
                            //double deltaWeight = tempWeight - n.InputWeights[InputNeurons.IndexOf(m) + 1];
                            //n.InputWeights[InputNeurons.IndexOf(m) + 1] = n.InputWeights[InputNeurons.IndexOf(m) + 1] + alpha * deltaWeight + p * n.Delta * m.Output;
                                n.InputWeights[InputNeurons.IndexOf(m) + 1] = n.InputWeights[InputNeurons.IndexOf(m) + 1] + p * n.Delta * m.Output;
                            }
                        }

                        foreach (Neuron n in OutputNeurons.ToList())
                        {
                            foreach (Neuron m in MiddleNeurons.ToList())
                            {
                            //double tempWeight = n.InputWeights[MiddleNeurons.IndexOf(m) + 1] + p * n.Delta * m.Output;
                            //double deltaWeight = tempWeight - n.InputWeights[MiddleNeurons.IndexOf(m) + 1];
                            //n.InputWeights[MiddleNeurons.IndexOf(m) + 1] = n.InputWeights[MiddleNeurons.IndexOf(m) + 1] + alpha * deltaWeight + p * n.Delta * m.Output;
                                n.InputWeights[MiddleNeurons.IndexOf(m) + 1] = n.InputWeights[MiddleNeurons.IndexOf(m) + 1] + p * n.Delta * m.Output;
                            }
                        }
                    }
                    StepCount++;

                    //Sprawdzanie błędu
                    if (StepCount >= 50)
                    {
                        Error = new double[] { 0, 0, 0, 0, 0 };
                        ErrorAVG = 0;
                        foreach (TrainingVector t in TrainingVectorList.ToList())
                        {
                            TempVector.SetInputs(t.GetInputs());
                            RecognizeVector();
                            t.OutputValues = TempVector.OutputValues;

                            ErrorAVG += (double)CompareAnsw(t) / TrainingVectorList.Count();
                        }


                        main.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            status.Content = "Współczynnik Błędu sieci neuronowej wynosi: " + ErrorAVG;
                            progbar.Value = 0.5 - ErrorAVG;
                        }));

                        StepCount = 0;
                    }

                }
        }

         
        public double CompareAnsw(TrainingVector t)
        {
            double ErrorA = 0;
            for(int i =0; i<answersQuantity; i++)
            {
                if (t.GetAnswer(i))
                {
                    Error[i] += Math.Abs(1 - t.OutputValues[i]);
                    ErrorA += (double)Error[i] / answersQuantity;
                } else
                {
                    Error[i] += Math.Abs(0 - t.OutputValues[i]);
                    ErrorA+= (double)Error[i] / answersQuantity;
                }
            }
            return ErrorA;
            
        }


        public void RecognizeVector()
        {
            TempVector.SetAnswers(new bool[] { false, false, false, false, false });
            //Zerowanie sum wazonych
            foreach (Neuron n in InputNeurons.ToList())
            {
                n.WeightedSum = 1;
            }
            foreach (Neuron n in MiddleNeurons.ToList())
            {
                n.WeightedSum = 1;
            }
            foreach (Neuron n in OutputNeurons.ToList())
            {
                n.WeightedSum = 1;
            }

            //Faza propagacji w przód
            foreach (Neuron n in InputNeurons.ToList())
            {
                int i = InputNeurons.IndexOf(n) / canvasNetWidth, j = InputNeurons.IndexOf(n) % canvasNetWidth;
                n.CalcWeightedSum(1, TempVector.GetInput(i, j));
                n.OutputFunction();
            }
            foreach (Neuron n in MiddleNeurons.ToList())
            {
                int index = 1;
                foreach (Neuron i in InputNeurons.ToList())
                {
                    n.CalcWeightedSum(index, i.Output);
                    index++;
                }
                n.OutputFunction();
            }
            foreach (Neuron n in OutputNeurons.ToList())
            {
                int index = 1;
                foreach (Neuron i in MiddleNeurons.ToList())
                {
                    n.CalcWeightedSum(index, i.Output);
                    index++;
                }
                n.OutputFunction();
                TempVector.OutputValues[OutputNeurons.IndexOf(n)] = n.Output;
                if (Math.Round(n.Output) == 0)
                {
                    TempVector.SetAnswer(OutputNeurons.IndexOf(n), false);
                }
                else
                {
                    TempVector.SetAnswer(OutputNeurons.IndexOf(n), true);
                }
            }


        }

        public void SetTempVectorInputs(System.Drawing.Bitmap bm)
        {
            int sum = 0;
            for (int x = 0; x < canvasNetWidth; x++)
            {
                for (int y = 0; y < canvasNetHeight; y++)
                {
                    for (int i = 0; i < WidthModulo; i++)
                    {
                        for (int j = 0; j < HeightModulo; j++)
                        {
                            System.Drawing.Color test = bm.GetPixel(WidthModulo * x + i, HeightModulo * y + j);
                            if (test == System.Drawing.Color.FromArgb(255, 2, 2, 2)) sum++;
                        }
                    }

                    TempVector.SetInput(y, x, Math.Round((double)sum / (WidthModulo * HeightModulo), 3));
                    sum = 0;
                }
            }
        }

        public Boolean[] getTempAnswers()
        {
            return TempVector.GetAnswers();
        }


        public void SetTempVectorSymbol(TextBox textBox)
        {
            TempVector.SetSymbol(textBox.Text);
        }


        public void SetTempVectorAnswers(int index)
        {
            String binindex = Convert.ToString(index, 2); //int do binary
            Boolean[] answ = new Boolean[answersQuantity];
            int diff = answersQuantity - binindex.Length;
            for (int i = 0; i < answersQuantity; i++)
            {
                if (i < binindex.Length)
                {
                    if (binindex.Substring(i, 1) == "1")
                    {
                        answ[answersQuantity - 1 - diff - i] = true;
                    }
                    else
                    {
                        answ[answersQuantity - 1 - diff - i] = false;
                    }
                }
                else
                {
                    answ[i] = false;
                }
            }
            TempVector.SetAnswers(answ);
        }


        public Boolean AddTemp2List()
        {
            Boolean same = false;
            foreach (TrainingVector t in TrainingVectorList.ToList())
            {
                if (t.GetNumOfInputs() == TempVector.GetNumOfInputs()) //Wykonaj dla tej samej liczby inputów w obu wektorach.
                {
                    int sameinputs = 0;
                    Boolean diff = true;
                    for (int i = 0; i < canvasNetWidth && diff; i++)
                    {
                        for (int j = 0; j < canvasNetHeight && diff; j++)
                        {
                            if (Math.Abs(t.GetInput(i, j) - TempVector.GetInput(i, j)) <= accuracy && (t.GetInput(i,j) !=0.0 && TempVector.GetInput(i,j) !=0.0)) //Jeżeli w obu inputach wektorów jest true.
                            {
                                ++sameinputs;
                            }
                            if (sameinputs == TempVector.GetNumOfInputs())
                            {
                                diff = false;
                                same = true;
                            }
                        }
                    }
                }
            }
            if (!same)
            {
                TrainingVectorList.Add(new TrainingVector(TempVector));
                return true;
            }
            else return false;
        }

        public void ReplaceAnswersInList(Boolean[] answ)
        {
            foreach (TrainingVector t in TrainingVectorList.ToList())
            {
                if (t.GetNumOfInputs() == TempVector.GetNumOfInputs() && t.getSymbol() == TempVector.getSymbol()) //Wykonaj dla tej samej liczby inputów w obu wektorach.
                {
                    int sameinputs = 0;
                    Boolean diff = true;
                    for (int i = 0; i < canvasNetWidth && diff; i++)
                    {
                        for (int j = 0; j < canvasNetHeight && diff; j++)
                        {
                            if (Math.Abs(t.GetInput(i, j) - TempVector.GetInput(i, j)) <= accuracy && t.GetInput(i, j) != 0.0 && TempVector.GetInput(i, j) != 0.0) //Jeżeli w obu inputach wektorów jest true.
                            {
                                ++sameinputs;
                            }
                            if (sameinputs == TempVector.GetNumOfInputs())
                            {
                                diff = false;
                                t.SetAnswers(answ);
                                t.SetSymbol(TempVector.getSymbol());
                            }
                        }
                    }
                }
            }
        }

        public void RemoveFromList(String symbol)
        {
            foreach (TrainingVector t in TrainingVectorList.ToList())
            {
                if (t.getSymbol() == symbol) TrainingVectorList.Remove(t);
            }
        }

        public void UpdateListAnswers(int index, String symbol)
        {
            foreach (TrainingVector t in TrainingVectorList.ToList())
            {
                if (t.getSymbol() == symbol)
                {
                    String binindex = Convert.ToString(index, 2); //int do binary
                    int diff = answersQuantity - binindex.Length;
                    for (int i = 0; i < answersQuantity; i++)
                    {
                        if (i < binindex.Length)
                        {
                            if (binindex.Substring(i, 1) == "1")
                            {
                                t.SetAnswer(answersQuantity - 1 - diff - i, true);
                            }
                            else
                            {
                                t.SetAnswer(answersQuantity - 1 - diff - i, false);
                            }
                        }
                        else
                        {
                            t.SetAnswer(i, false);
                        }
                    }
                }
            }
        }

        public void ClearTempVector()
        {
            TempVector.ClearAll();
        }

        internal void SaveNetwork(ItemCollection list)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "NNetwork"; // Domyślna nazwa pliku
            dlg.DefaultExt = ".nn"; // Domyślne rozszerzenie
            dlg.Filter = "Neural Network (.nn)|*.nn"; // Filtr rozszerzeń

            // Pokaż okienko dialogowe
            Nullable<bool> result = dlg.ShowDialog();

            // Jeśli okno poprawnie się otworzyło
            if (result == true)
            {
                // Wybranie pliku
                string filename = dlg.FileName;
                //Działanie na pliku
                string text = "";
                ////Wpisywanie symboli
                //for (int i = 0; i < list.Count; i++) 
                //{
                //    text += list.GetItemAt(i).ToString();
                //    if(i != list.Count - 1) text += " ";
                //}
                //text += (char)11;//Znak końca listy symboli (vertical tab)
                //            //Wpisywanie wejść i odpowiedzi po kolei
                foreach (TrainingVector t in TrainingVectorList)
                {
                    text += t.getSymbol();

                    text += (char)11;

                    for (int i = answersQuantity - 1; i >= 0; i--)
                    {
                        if (t.GetAnswer(i) == true) text += '1';
                        else text += '0';
                    }

                    text += (char)11;

                    for (int i = 0; i < canvasNetWidth; i++)
                    {
                        for (int j = 0; j < canvasNetHeight; j++)
                        {
                            Double number = t.GetInput(i, j) * 1000;
                            int num1 = (int)number;
                            int diff = 4 - number.ToString().Length;
                            for (int z = 0; z < diff; z++) text += "0";
                            text += number.ToString();
                        }
                    }

                    text += (char)11;
                    text += t.GetNumOfInputs();
                    text += "\n";
                }
                System.IO.File.WriteAllText(filename, text);
            }
        }


        internal void SaveWeights()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "NNetworkWeights"; // Domyślna nazwa pliku
            dlg.DefaultExt = ".nw"; // Domyślne rozszerzenie
            dlg.Filter = "Neural Network Weights (.nw)|*.nw"; // Filtr rozszerzeń

            // Pokaż okienko dialogowe
            Nullable<bool> result = dlg.ShowDialog();

            // Jeśli okno poprawnie się otworzyło
            if (result == true)
            {
                // Wybranie pliku
                string filename = dlg.FileName;
                //Działanie na pliku
                string text = "";

                foreach (Neuron n in InputNeurons.ToList())
                {
                    for (int i = 1; i < n.InputWeightCount + 1; i++)
                    {
                        text += Math.Round(n.InputWeights[i], 15);
                        text += (char)11;
                    }
                    text += "\n";
                }
                text += "\n";
                foreach (Neuron n in MiddleNeurons.ToList())
                {
                    for (int i = 1; i < n.InputWeightCount + 1; i++)
                    {
                        text += Math.Round(n.InputWeights[i], 15);
                        text += (char)11;
                    }
                    text += "\n";
                }
                text += "\n";
                foreach (Neuron n in OutputNeurons.ToList())
                {
                    for (int i = 1; i < n.InputWeightCount + 1; i++)
                    {
                        text += Math.Round(n.InputWeights[i], 15);
                        text += (char)11;
                    }
                    text += "\n";
                }
                System.IO.File.WriteAllText(filename, text);
            }
        }

        public void TrimTempVectorInputs()
        {
            PushInputsLeft();
            PushInputsUp();
        }


        private void PushInputsLeft()
        {
            int count = 0;
            bool t = true;
            for (int i = 0; i < canvasNetWidth && t; i++)
            {
                for (int j = 0; j < canvasNetHeight; j++)
                {
                    if (TempVector.GetInput(j, i) > 0)
                    {
                        t = false;
                        break;
                    }
                }
                if (t)
                    count++;
            }

            for (int i = 0; i < count; i++)
            {
                for (int x = 0; x < canvasNetWidth - 1; x++)
                {
                    for (int y = 0; y < canvasNetHeight; y++)
                    {
                        TempVector.SetInput(y, x, TempVector.GetInput(y, x + 1));
                        if (x == canvasNetWidth - 2)
                        {
                            TempVector.SetInput(y, x + 1, 0);
                        }
                    }
                }
            }
        }

        private void PushInputsUp()
        {
            int count = 0;
            bool t = true;
            for (int i = 0; i < canvasNetHeight && t; i++)
            {
                for (int j = 0; j < canvasNetWidth; j++)
                {
                    if (TempVector.GetInput(i, j) > 0)
                    {
                        t = false;
                        break;
                    }
                }
                if (t)
                    count++;
            }

            for (int i = 0; i < count; i++)
            {
                for (int y = 0; y < canvasNetHeight - 1; y++)
                {
                    for (int x = 0; x < canvasNetWidth; x++)
                    {
                        TempVector.SetInput(y, x, TempVector.GetInput(y + 1, x));
                        if (y == canvasNetHeight - 2)
                        {
                            TempVector.SetInput(y + 1, x, 0);
                        }
                    }
                }
            }
        }

        internal void LoadNetwork(Label status, ItemCollection symbollist, Canvas canvas, TextBox textbox)
        {
            MessageBoxResult resultW = MessageBox.Show("Wczytywanie sieci usunie wszystkie bieżące dane!\n Czy jesteś pewien?", "Wczytanie sieci z pliku", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (resultW == MessageBoxResult.Yes)
            {
                symbollist.Clear();
                TrainingVectorList.Clear();
                canvas.Children.Clear();
                textbox.Clear();

                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.FileName = "NNetwork"; // Default file name
                dlg.DefaultExt = ".nn"; // Default file extension
                dlg.Filter = "Neural Network (.nn)|*.nn"; // Filter files by extension

                // Show open file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process open file dialog box results
                if (result == true)
                {
                    // Open document
                    string filename = dlg.FileName;
                    try
                    {   // Open the text file using a stream reader.
                        using (StreamReader sr = new StreamReader(filename))
                        {
                            // Read the stream to a string, and write the string to the console.
                            while (!sr.EndOfStream)
                            {
                                TempVector.ClearAll();
                                int anit = answersQuantity - 1;
                                int inx = 0;
                                int iny = 0;
                                int numofdividers = 0;
                                String symb = null;
                                String inp = null;
                                int inpi = 0;
                                Boolean symbset = false;
                                char divide = (char)11;
                                String line = sr.ReadLine();
                                for (int i = 0; i < line.Count(); i++)
                                {
                                    if (line.ElementAt(i) == divide) //sprawdzanie czy podzialka
                                    {
                                        ++numofdividers;
                                        continue;
                                    }
                                    if (numofdividers == 0) //zapisywanie nazwy symbolu
                                    {
                                        symb += line.ElementAt(i).ToString();

                                    }
                                    else if (numofdividers == 1) //wpisanie symbolu i zapisywanie odpowiedzi
                                    {
                                        if (!symbset)
                                        {
                                            TempVector.SetSymbol(symb);
                                            if (!symbollist.Contains(symb)) symbollist.Add(symb);
                                            symbset = true;
                                        }
                                        if (line.ElementAt(i) == '1')
                                        {
                                            TempVector.SetAnswer(anit, true);
                                            anit--;
                                        }
                                        else
                                        {
                                            TempVector.SetAnswer(anit, false);
                                            anit--;
                                        }
                                    }
                                    else if (numofdividers == 2) //wpisywanie wejść
                                    {
                                        if (inx > canvasNetWidth - 1)
                                        {
                                            inx = 0;
                                            iny++;
                                        }

                                        if (inpi == 4)
                                        {
                                            TempVector.SetInput(iny, inx, (Double)(Double.Parse(inp) / 1000));
                                            inpi = 0;
                                            inp = null;
                                            inx++;
                                        }

                                        inp += line.ElementAt(i);
                                        inpi++;
                                        //if (line.ElementAt(i) == '1')
                                        //{
                                        //    TempVector.SetInput(inx, iny, true);
                                        //    inx++;
                                        //}
                                        //else
                                        //{
                                        //    TempVector.SetInput(inx, iny, false);
                                        //    inx++;
                                        //}
                                    }
                                    else if (numofdividers == 3) //wpisywanie ilości zaznaczonych wejść
                                    {
                                        TempVector.SetNumOfInputs(line.ElementAt(i));
                                    }
                                }
                                AddTemp2List();
                                ErrorAVG = 99;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
        }



        internal void LoadWeights(Boolean train)
        {
            Boolean pom1 = true;
            if (train)
            {
                MessageBoxResult resultW = MessageBox.Show("Siec została już wytrenowana. Czy chcesz nadpisać wagi?", "Wczytanie wag z pliku", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (resultW == MessageBoxResult.No)
                {
                    pom1 = false;
                }
            }

            if (pom1)
            {
                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.FileName = "NNetworkWeights"; // Domyślna nazwa pliku
                dlg.DefaultExt = ".nw"; // Domyślne rozszerzenie
                dlg.Filter = "Neural Network Weights (.nw)|*.nw"; // Filtr rozszerzeń

                // Show open file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process open file dialog box results
                if (result == true)
                {
                    // Open document
                    string filename = dlg.FileName;
                    try
                    {   // Open the text file using a stream reader.
                        using (StreamReader sr = new StreamReader(filename))
                        {
                            int pom = 0;
                            int neuronit = 0;
                            // Read the stream to a string, and write the string to the console.
                            while (!sr.EndOfStream)
                            {
                                String line = sr.ReadLine();
                                if (line.Equals(""))
                                {
                                    pom++;
                                    neuronit = 0;
                                }
                                else
                                {
                                    String weight = "";
                                    int weightit = 1;
                                    for (int i = 0; i < line.Count(); i++)
                                    {
                                        if (line.ElementAt(i) == (char)11)
                                        {
                                            if (pom == 0)
                                            {
                                                InputNeurons[neuronit].InputWeights[weightit] = Double.Parse(weight);
                                            }
                                            else if (pom == 1)
                                            {
                                                MiddleNeurons[neuronit].InputWeights[weightit] = Double.Parse(weight);
                                            }
                                            else
                                            {
                                                OutputNeurons[neuronit].InputWeights[weightit] = Double.Parse(weight);
                                            }
                                            weightit++;
                                            weight = "";
                                        }
                                        else weight += line.ElementAt(i);
                                    }
                                    neuronit++;
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
        }
    }
}
