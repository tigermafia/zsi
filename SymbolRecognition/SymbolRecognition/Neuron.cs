﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymbolRecognition
{
    class Neuron
    {
        private Random rand;
        public List<double> InputWeights = new List<double>();
        public int InputWeightCount { get; set; }
        public double Output { get; set; }
        public double WeightedSum { get; set; }
        public double Deriv { get; set; }
        public double Delta { get; set; }

        public Neuron(int InputWeightCount, Random rand)
        {
            InputWeights.Add(1);
            this.InputWeightCount = InputWeightCount;
            this.rand = rand;
            SetRandomWeights();
            WeightedSum = 1;
        }

        private void SetRandomWeights()
        {
            for (int i = 0; i < InputWeightCount; i++)
            {
                InputWeights.Add(rand.NextDouble()*2-1);
            }
        }

        public void OutputFunction() //Funkcja wyjścia (ta z eulerem) pobiera sume ważoną jako parametr
        {
            Output = 1 / (1 + Math.Pow(Math.E, -WeightedSum));
            Deriv = Output * (1 - Output);
        }

        public void CalcWeightedSum(int index, double Input)
        {
            WeightedSum += Input * InputWeights[index];
        }

    }
}
