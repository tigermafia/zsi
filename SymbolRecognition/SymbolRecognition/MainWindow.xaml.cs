﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.ComponentModel;
using System.Windows.Shapes;

namespace SymbolRecognition
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SolidColorBrush ColorMG = new SolidColorBrush(Color.FromArgb(100, 43, 59, 75)); //Ciemny granat (Menu gorne)
        private SolidColorBrush PenColor = new SolidColorBrush(Color.FromArgb(255, 2, 2, 2));
        private SolidColorBrush ColorBG = Brushes.LightSteelBlue;//Kolor tła (Jasny szary)
        private ItemCollection SymbolList;
        private NeuralNetwork NNetwork = null;
        private Boolean trained = false;
        private Boolean recognized = false;
        private Boolean VectorNEmpty = true;
        private WriteableBitmap wbm;
        private System.Drawing.Bitmap bm;
        private readonly BackgroundWorker worker = new BackgroundWorker();
        private AppDomain currentDomain = AppDomain.CurrentDomain;
        //private Point TrainCanvasPoint;
        private int MaxSymbolListSize = 32;


        //Konstruktor
        public MainWindow()
        {
            InitializeComponent();
            this.Measure(new Size(Width, Height));
            this.Arrange(new Rect(0, 0, this.Width, this.Height));
            SymbolList = SymbolListBox.Items;
            RecoListBox.ItemsSource = SymbolList;
            GridIntro.Visibility = Visibility.Visible;
            GridTrain.Visibility = Visibility.Hidden;
            GridReco.Visibility = Visibility.Hidden;
            NNetwork = new NeuralNetwork();
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(WindowHandler);

            worker.WorkerSupportsCancellation = true;

            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;

            StatusLabel.Content = "Strona tytułowa.";
            TrainCanvas.Loaded += ControlLoaded;

        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            NNetwork.TrainNetwork(StatusLabel, this, StatusBar);
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Trenowanie zakończone!", "Trenowanie", MessageBoxButton.OK, MessageBoxImage.Information);
            trained = true;
        }

        //Canvas -> WriteableBitmap
        public void SaveAsWriteableBitmap(Canvas can)
        {
            // Save current canvas transform
            //Transform transform = TrainCanvas.LayoutTransform;
            //// reset current transform (in case it is scaled or rotated)
            //TrainCanvas.LayoutTransform = null;

            // Get the size of canvas
            Size size = new Size(can.ActualWidth, can.ActualHeight);
            // Measure and arrange the surface
            // VERY IMPORTANT
            // TrainCanvas.Measure(size);

            // Create a render bitmap and push the surface to it
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
              (int)size.Width,
              (int)size.Height,
              96d,
              96d,
              PixelFormats.Pbgra32);
            renderBitmap.Render(can);


            //Restore previously saved layout
            //TrainCanvas.LayoutTransform = transform;

            //create and return a new WriteableBitmap using the RenderTargetBitmap
            wbm = new WriteableBitmap(renderBitmap);

        }


        private void BitmapFromWriteableBitmap()
        {
            using (System.IO.MemoryStream outStream = new System.IO.MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((BitmapSource)wbm));
                enc.Save(outStream);
                bm = new System.Drawing.Bitmap(outStream);
            }
        }




        //Wyświetlanie nieobsługiwanych wyjątków
        public static void WindowHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            MessageBoxResult result = MessageBox.Show("Nieobsługiwany wyjątek:\n" + e.Message, e.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            if (result == MessageBoxResult.OK)
            {
                Application.Current.Shutdown();
            }

        }


        public void ControlLoaded(object sender, EventArgs e)
        {
            //TrainCanvasPoint = TrainCanvas.TransformToAncestor(Application.Current.MainWindow)
            //              .Transform(new Point(0, 0));
            NNetwork.SetNetwork(TrainCanvas.ActualWidth, TrainCanvas.ActualHeight);
        }

        //Wyjście z programu
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Czy chcesz zamknąć program?", "Wyjście", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }

        }


        //Przesuwanie okna przy użyciu menu górnego
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        //Minimalizacja okna
        private void MinButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }


        //Wybor Trenowania
        private void MenuButton1_Click(object sender, RoutedEventArgs e)
        {
            TrainSelectRect.Fill = ColorBG;
            RecoSelectRect.Fill = ColorMG;
            GridTrain.Visibility = Visibility.Visible;
            GridReco.Visibility = Visibility.Hidden;
            GridIntro.Visibility = Visibility.Collapsed;

            StatusLabel.Content = "Wybór \"Trenowanie\".";
        }


        //Wybor Rozpoznawania
        private void MenuButton2_Click(object sender, RoutedEventArgs e)
        {
            TrainSelectRect.Fill = ColorMG;
            RecoSelectRect.Fill = ColorBG;
            GridTrain.Visibility = Visibility.Hidden;
            GridReco.Visibility = Visibility.Visible;
            GridIntro.Visibility = Visibility.Collapsed;

            StatusLabel.Content = "Wybór \"Rozpoznawanie\".";
        }


        //Wybor Wyrazow
        private void MenuButton3_Click(object sender, RoutedEventArgs e)
        {
            TrainSelectRect.Fill = ColorMG;
            RecoSelectRect.Fill = ColorMG;
            GridTrain.Visibility = Visibility.Hidden;
            GridReco.Visibility = Visibility.Hidden;
            GridIntro.Visibility = Visibility.Collapsed;

            StatusLabel.Content = "Wybór rozpoznawania wyrazów.";
        }

        //Dodawanie nowych wektorów trenujących
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            SaveAsWriteableBitmap(TrainCanvas);
            BitmapFromWriteableBitmap();
            NNetwork.SetTempVectorInputs(bm);
            NNetwork.TrimTempVectorInputs();
            if (SymbolListBox.Items.Count < MaxSymbolListSize)
            {
                if (!SymbolTextBox.Text.Equals(""))
                {
                    if (TrainCanvas.Children.Count != 0)
                    {
                        bool t = false;
                        int index = 0;
                        for (int i = 0; i < SymbolListBox.Items.Count; i++)
                        {
                            if (SymbolTextBox.Text.Equals(SymbolListBox.Items.GetItemAt(i).ToString()))
                            {
                                t = true;
                                index = i;
                                break;
                            }
                        }
                        if (!t)
                        {
                            SymbolList.Add(SymbolTextBox.Text);
                            NNetwork.SetTempVectorSymbol(SymbolTextBox);
                            NNetwork.SetTempVectorAnswers(SymbolList.Count - 1);
                            if (NNetwork.AddTemp2List())
                            {
                                VectorNEmpty = false;
                                StatusLabel.Content = "Dodano \"" + SymbolTextBox.Text + "\" do listy symboli.";
                                SymbolTextBox.Clear();
                                TrainCanvas.Children.Clear();
                                NNetwork.ErrorAVG = 99;
                            } else
                            {
                                SymbolList.RemoveAt(SymbolList.Count - 1);
                                StatusLabel.Content = "Błąd!";
                                MessageBoxResult result = MessageBox.Show("Na liście istnieje tak narysowany symbol!", "Trenowanie - Dodaj symbol", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                TrainCanvas.Children.Clear();
                            }
                            
                            NNetwork.ClearTempVector();
                        }
                        else
                        {
                            NNetwork.SetTempVectorSymbol(SymbolTextBox);
                            NNetwork.SetTempVectorAnswers(index);
                            if (NNetwork.AddTemp2List())
                            {
                                VectorNEmpty = false;
                                StatusLabel.Content = "Dodano kolejny wektor trenujący dla \"" + SymbolTextBox.Text + "\"";
                                SymbolTextBox.Clear();
                                TrainCanvas.Children.Clear();
                                NNetwork.ErrorAVG = 99;
                            } else
                            {
                                StatusLabel.Content = "Błąd!";
                                MessageBoxResult result = MessageBox.Show("Na liście istnieje tak narysowany symbol!", "Trenowanie - Dodaj symbol", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                TrainCanvas.Children.Clear();
                            }
                            NNetwork.ClearTempVector();
                        }
                    } else
                    {
                        StatusLabel.Content = "Błąd!";
                        MessageBoxResult result = MessageBox.Show("Pole do rysowania jest puste!", "Trenowanie - Dodaj symbol", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    StatusLabel.Content = "Błąd!";
                    MessageBoxResult result = MessageBox.Show("Pole \"Symbol\" jest puste!", "Trenowanie - Dodaj symbol", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            } else
            {
                StatusLabel.Content = "Błąd!";
                MessageBoxResult result = MessageBox.Show("Lista symboli jest pełna!", "Trenowanie - Dodaj symbol", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        //Usuwanie symboli
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (SymbolListBox.SelectedItem != null)
            {
                String selected = SymbolListBox.SelectedItem as String;
                int selindex = SymbolListBox.SelectedIndex;
                StatusLabel.Content = "Usunięto \"" + selected + "\" z listy symboli.";
                NNetwork.RemoveFromList(selected);
                SymbolList.RemoveAt(SymbolListBox.SelectedIndex);
                for(int i =selindex; i<SymbolList.Count; i++)
                {
                    NNetwork.UpdateListAnswers(i, SymbolListBox.Items.GetItemAt(i).ToString());
                }

            }
        }

        //Rysowanie
        private void CanvasNet(object sender, MouseEventArgs e)
        {
            Ellipse el = new Ellipse();
            el.Width = 9;
            el.Height = 9;
            el.Fill = PenColor;
            
            Point p = Mouse.GetPosition((Canvas)sender);

            //Rysowanie
            if (MouseButtonState.Pressed == e.LeftButton && p.X<((Canvas)sender).ActualWidth - el.Width / 2 - 1 && p.X >= el.Width / 2 && p.Y<((Canvas)sender).ActualHeight - el.Height / 2 && p.Y >= el.Height / 2 - 1)
            {
                StatusLabel.Content = (int)p.X + "x" + (int)p.Y + "y";
                Canvas.SetLeft(el, p.X - el.Width / 2);
                Canvas.SetTop(el, p.Y - el.Height / 2);

                ((Canvas)sender).Children.Add(el);
                StatusLabel.Content = "Rysowanie.";
            }


        } 

        //Czyszczenie canvasu
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            TrainCanvas.Children.Clear();
            SymbolTextBox.Clear();
            NNetwork.ClearTempVector();
            StatusLabel.Content = "Wyczyszczenie pola.";
        }

        private void RecoClearButton_Click(object sender, RoutedEventArgs e)
        {
            RecoCanvas.Children.Clear();
            RecoSymbTextBox.Text = "";
            StatusLabel.Content = "Wyczyszczenie pola.";
            recognized = false;
        }

        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {
            HelpWindow help = new HelpWindow();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            NNetwork.SaveNetwork(SymbolList);
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            NNetwork.LoadNetwork(StatusLabel, SymbolList, TrainCanvas, SymbolTextBox);
            VectorNEmpty = false;
        }

        private void TrainButton_Click(object sender, RoutedEventArgs e)
        {
            if (NNetwork.TrainingVectorList.Count != 0)
            {
                if (!worker.IsBusy)
                {
                    worker.RunWorkerAsync();
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show("Trenowanie w toku. Czy chcesz je zakończyc?", "Trenowanie", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    
                    if (result == MessageBoxResult.Yes)
                    {
                        worker.CancelAsync();
                        NNetwork.cancel = worker.CancellationPending;
                    }
                }
            } else
            {
                StatusLabel.Content = "Błąd!";
                MessageBox.Show("Niemożliwe trenowanie bez wektorów", "Trenowanie - Trenuj", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void RecoButton_Click(object sender, RoutedEventArgs e)
        {
            SaveAsWriteableBitmap(RecoCanvas);
            BitmapFromWriteableBitmap();
            NNetwork.TempVector = new TrainingVector(NNetwork.canvasNetWidth, NNetwork.canvasNetHeight, NNetwork.answersQuantity);
            NNetwork.SetTempVectorInputs(bm);
            NNetwork.TrimTempVectorInputs();

            Boolean found = false;

            if(trained)
            {
                NNetwork.RecognizeVector();
                String odp = "";
                for(int i= NNetwork.getTempAnswers().Count()-1; i>=0;i--)
                {
                    if (NNetwork.TempVector.GetAnswer(i))
                    {
                        odp += "1";
                    }
                    else
                    {
                        odp += "0";
                    }
                }

                foreach (String it in SymbolList)
                {
                    
                    if(SymbolList.IndexOf(it)==Convert.ToInt32(odp,2))
                    {
                        RecoSymbTextBox.Text = it;
                        found = true;
                        StatusLabel.Content = "Rozpoznano!";
                        recognized = true;
                    }
                }
                if(!found)
                {
                    StatusLabel.Content = "Odpowiedź";
                    MessageBox.Show("Nieznaleziono odpowiedzi!", "Rozpoznawanie - Rozpoznaj", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            } else
            {
                StatusLabel.Content = "Błąd!";
                MessageBox.Show("Niewytrenowana sieć!", "Rozpoznawanie - Rozpoznaj", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void LoadWButton_Click(object sender, RoutedEventArgs e)
        {
            NNetwork.LoadWeights(trained);
            trained = true;
        }

        private void SaveWButton_Click(object sender, RoutedEventArgs e)
        {
            NNetwork.SaveWeights();
        }

        private void RecoTrain_Click(object sender, RoutedEventArgs e)
        {
            if (recognized)
            {
                if (RecoListBox.SelectedIndex != -1)
                {
                    SaveAsWriteableBitmap(RecoCanvas);
                    BitmapFromWriteableBitmap();
                    NNetwork.SetTempVectorInputs(bm);
                    NNetwork.TrimTempVectorInputs();
                    NNetwork.SetTempVectorAnswers(RecoListBox.SelectedIndex);
                    NNetwork.TempVector.SetSymbol(RecoListBox.SelectedItem.ToString());

                    if (NNetwork.AddTemp2List())
                    {
                        MessageBox.Show("Dodano nowy wektor trenujący.", "Rozpoznawanie - Popraw", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        NNetwork.ReplaceAnswersInList(NNetwork.TempVector.GetAnswers());
                        MessageBox.Show("Zmieniono odpowiedź dla podanego wektora.", "Rozpoznawanie - Popraw", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    NNetwork.ErrorAVG = 99;
                    trained = false;

                }
                else
                {
                    StatusLabel.Content = "Błąd!";
                    MessageBox.Show("Zaznacz symbol z listy!", "Rozpoznawanie - Popraw", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Najpierw rozpoznaj narysowany symbol!", "Rozpoznawanie - Popraw", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
